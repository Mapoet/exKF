
#include <iostream>
#include <random>
#include <numeric>
#include <tuple>
#include "mgl2/glut.h"
#include "exKF.hpp"
int main(int argc, char **argv)
{
    typedef exKF::exKF<double> exKF;
    double lambda = argc >= 2 ? atof(argv[1]) : 0.01, beta = argc >= 3 ? atof(argv[2]) : 50;
    exKF::Array x0 = {0.0, 0.0, 0.1, 0.2}, p = {0.0}, theory = {0.0, 0.0}, measure = {0.0, 0.0};
    exKF::Matrix P, Q, R, xc;
    P.setZero(4, 4);
    R.setZero(2, 2);
    P(0, 0) = 1.0;
    P(1, 1) = 1.0;
    P(2, 2) = 1.0 * 0.1 * 0.1;
    P(3, 3) = 1.0 * 0.1 * 0.1;
    Q = lambda * P;
    R(0, 0) = beta * beta;
    R(1, 1) = beta * beta;
    exKF kf(x0, P, p, [](const exKF::DiffArray &argsin, const exKF::Array &parameter) {
        exKF::DiffArray argsout(4);
        argsout[0] = argsin[0] + argsin[2] * parameter[1];
        argsout[1] = argsin[1] + argsin[3] * parameter[1];
        argsout[2] = argsin[2];
        argsout[3] = argsin[3];
        return argsout;
    });
    typedef std::tuple<exKF, Eigen::MatrixXd, Eigen::MatrixXd, double> Data;
    Data data = std::make_tuple(kf, Q, R, beta);
    auto gl = new mglGLUT([](HMGL hmgr, void *v) {
        exKF::Array p = {0.0}, theory = {0.0, 0.0}, measure = {0.0, 0.0};
        exKF::Matrix xc;
        std::random_device rd;
        std::mt19937 gen(rd());
        std::normal_distribution<double> randn;
        auto gr = new mglGraph(hmgr);
        Data data = *((Data *)v);
        auto kf = std::get<0>(data);
        auto Q = std::get<1>(data);
        auto R = std::get<2>(data);
        auto beta = std::get<3>(data);
        std::vector<double> xt, yt, xm, ym, xa, ya;
        for (auto i = 0; i < 10000; i += 50)
        {
            for (auto j = i; j < i+50; j++)
            {
                p[0] = 0.1 * j;
                kf.predict(p, Q);
                theory[0] = 0.1 * j * sin(p[0] / 300 * 3.14159265) * (1 + randn(gen) * 0.001) + 0.1 * j;
                theory[1] = 0.2 * j * sin(p[0] / 500 * 3.14159265) * (1 + randn(gen) * 0.001) + 0.2 * j;
                measure[0] = theory[0] + randn(gen) * beta;
                measure[1] = theory[1] + randn(gen) * beta;
                kf.update_linear(measure, R, [](const exKF::DiffArray &argsin, const exKF::Array &parameter) {
                    exKF::DiffArray argsout(2);
                    argsout[0] = argsin[0];
                    argsout[1] = argsin[1];
                    return argsout;
                });
                xc = kf.getState();
                //std::cout << theory[0] << "\t" << theory[1] << "\t" << measure[0] << "\t" << measure[1] << "\t" << xc(0, 0) << "\t" << xc(1, 0) << "\n";
                xt.emplace_back(theory[0]);
                yt.emplace_back(theory[1]);
                xm.emplace_back(measure[0]);
                ym.emplace_back(measure[1]);
                xa.emplace_back(xc(0, 0));
                ya.emplace_back(xc(1, 0));
            }
            gr->NewFrame();
            gr->SetOrigin(0, 0);
            gr->SetRanges(0, 1500, 0, 1500);
            gr->Plot(mglDataS(xm), mglDataS(ym), "y.4");
            gr->Plot(mglDataS(xt), mglDataS(yt), "b.4");
            gr->Plot(mglDataS(xa), mglDataS(ya), "r.4");
            gr->Grid();
            gr->Axis();
            gr->EndFrame();
        }
        return gr->GetNumFrame();
    },
                          "2D Kalman Filter", &data);
    gl->Run();

    return 0;
}
