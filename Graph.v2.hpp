
#include <memory>
#include <map>
#include <thread>
#include <tuple>
#include <variant>
#include <mutex>
#include "json.hpp"
namespace exKF
{
    typedef nlohmann::json Json;
    typedef enum __GTNAStatus
    {
        GTNA_Valued,
        GTNA_Trained
    } GTNAStatus;

    template <typename Scale, typename Flow>
    class NodeActive
    {
        Json _args;
    };
    template <typename Scale, typename Flow>
    class LinkActive
    {
        Json _value;
    };
    template <typename Scale, typename Flow>
    class CtrlActive
    {
        Json _value;
    };
    template <typename Scale, typename Flow>
    class CellActive
    {
        Json _value;
    };
    template <typename Scale, typename Flow>
    class StreamActive
    {
        Json _value;
    };
    template <typename Scale, typename Flow>
    class NodeValue
    {
        Json _value;
    };
    template <typename Scale, typename Flow>
    class CellValue
    {
        Json _value;
    };

    template <typename Key, typename Scale, typename Flow, typename NodeValue, typename NodeActive, typename LinkActive, typename CtrlActive, typename CellValue, typename CellActive, typename StreamActive>
    class TREE
    {
    public:
        typedef TREE<Key, Scale, Flow, NodeValue, NodeActive, LinkActive, CtrlActive, CellValue, CellActive, StreamActive> Tree;
        typedef std::shared_ptr<Tree> This;
        template <typename Key, typename Scale, typename Flow, typename NodeValue, typename NodeActive, typename LinkActive, typename CtrlActive, typename CellValue, typename CellActive, typename StreamActive>
        class NODE
        {
        public:
            typedef NODE<Key, Scale, Flow, NodeValue, NodeActive, LinkActive, CtrlActive, CellValue, CellActive, StreamActive> Node;
            template <typename Key, typename Scale, typename Flow, typename CellValue, typename CellActive, typename StreamActive>
            class CELL
            {
            public:
                typedef CELL<Key, Scale, Flow, CellValue, CellActive, StreamActive> Self;
                typedef std::shared_ptr<Self> This;
                typedef class _CellIStream
                {
                public:
                    std::pair<Self *, Self *> _iostream;
                    GTNAStatus _status;
                } * CellIStream;
                typedef class _CellOStream
                {
                public:
                    std::pair<std::pair<Self *, std::shared_ptr<CellValue>>, std::pair<Self *, std::shared_ptr<CellValue>>> _iostream;
                    std::shared_ptr<StreamActive> _active;
                    GTNAStatus _status;
                } * CellOStream;
                class CellKernel
                {
                public:
                    std::pair<std::vector<CellIStream>, std::vector<CellOStream>> _iostream;
                    std::shared_ptr<CellActive> _active;
                };

            //private:
                typename Node::NodeCtrl *_root;
                Json _args;
                CellKernel _active;
                mutable std::tuple<GTNAStatus, std::thread, std::timed_mutex> _thread;
            };

            typedef std::shared_ptr<Node> This;
            typedef std::shared_ptr<CELL<Key, Scale, Flow, CellValue, CellActive, StreamActive>> Leaf;

            class NodeKernel
            {
            public:
                typedef typedef class _NodeILink
                {
                public:
                    std::pair<Node, Node> _iostream;
                    GTNAStatus _status;
                } * NodeILink;
                typedef class _NodeOLink
                {
                public:
                    std::pair<std::pair<Node *, std::shared_ptr<NodeValue>>, std::pair<Node *, std::shared_ptr<NodeValue>>> _iostream;
                    std::shared_ptr<LinkActive> _active;
                    GTNAStatus _status;
                } * NodeOLink;
                std::pair<std::vector<NodeILink>, std::vector<NodeOLink>> _iostream;
                std::shared_ptr<NodeActive> _active;
            };
            template <typename Key, typename Cell, typename CtrlActive>
            class NodeCtrl
            {
            public:
                Node _root;
                std::map<Cell, std::shared_ptr<CtrlActive>> _nodes;
            };

        //private:
            std::variant<std::map<Key, Leaf>,
                         std::map<Key, This>>
                _next;
            std::variant<Tree *, Node *> _root;
            Json _args;
            NodeKernel _active;
            NodeCtrl<Key, Leaf, CtrlActive> _ctrl;
            mutable std::tuple<GTNAStatus, std::thread, std::timed_mutex> _thread;
        };
        typedef std::shared_ptr<NODE<Key, Scale, Flow, NodeValue, NodeActive, LinkActive, CtrlActive, CellValue, CellActive, StreamActive>> Node;
        typedef std::shared_ptr<Tree> This;

    //private:
        std::variant<std::map<Key, Node>,
                     std::map<Key, This>>
            _next;
        Tree *_root;
        Json _args;
    };
    template <typename Key, typename Scale, typename Flow>
    using KeyTree = TREE<Key, Scale, Flow, NodeValue<Scale, Flow>, NodeActive<Scale, Flow>, LinkActive<Scale, Flow>,
                         CtrlActive<Scale, Flow>, CellValue<Scale, Flow>, CellActive<Scale, Flow>, StreamActive<Scale, Flow>>;

} // namespace exKF