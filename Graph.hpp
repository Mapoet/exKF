
#include <memory>
#include <map>
#include <string>
#include <thread>
#include <tuple>
#include <variant>
#include <mutex>
#include "json.hpp"
namespace exKF
{
    typedef nlohmann::json Json;
    typedef enum __GTNAStatus
    {
        GTNA_Valued,
        GTNA_Trained
    } GTNAStatus;

    template <typename Scale, typename Flow>
    class NodeActive
    {
        Json _args;
    };
    template <typename Scale, typename Flow>
    class LinkActive
    {
        Json _value;
    };
    template <typename Scale, typename Flow>
    class NodeCtrl
    {
    public:
        Json _value;
    };
    template <typename Scale, typename Flow>
    class CellCtrl
    {
    public:
        Json _value;
    };
    template <typename Scale, typename Flow>
    class CellActive
    {
        Json _value;
    };
    template <typename Scale, typename Flow>
    class StreamActive
    {
        Json _value;
    };
    template <typename Scale, typename Flow>
    class IStream
    {
        Json _value;
    };
    template <typename Scale, typename Flow>
    class OStream
    {
        Json _value;
    };
    template <typename Scale, typename Flow>
    class NodeValue
    {
        Json _value;
    };
    template <typename Scale, typename Flow>
    class CellValue
    {
        Json _value;
    };

    template <typename Key, typename Scale, typename Flow, typename NodeCtrl,
              typename NodeValue, typename NodeActive, typename LinkActive, typename CellCtrl,
              typename CellValue, typename CellActive, typename StreamActive,
              typename IStream, typename OStream>
    class TREE
    {
    public:
        typedef TREE<Key, Scale, Flow, NodeCtrl, NodeValue, NodeActive, LinkActive, CellCtrl, CellValue, CellActive, StreamActive, IStream, OStream> Tree;
        typedef std::shared_ptr<Tree> This;
        template <typename Keys, typename Scales, typename Flows, typename NodeCtrls,
                  typename NodeValues, typename NodeActives, typename LinkActives, typename CellCtrls,
                  typename CellValues, typename CellActives, typename StreamActives,
                  typename IStreams, typename OStreams>
        class NODE
        {
        public:
            typedef NODE<Keys, Scales, Flows, NodeCtrls, NodeValues, NodeActives, LinkActives, CellCtrls, CellValues, CellActives, StreamActives, IStreams, OStreams> Node;
            template <typename Keyt, typename Scalet, typename Flowt, typename CellValuet, typename CellActivet, typename StreamActivet, typename IStreamt, typename OStreamt>
            class CELL
            {
            public:
                typedef CELL<Keyt, Scalet, Flowt, CellValuet, CellActivet, StreamActivet, IStreamt, OStreamt> Self;
                typedef std::shared_ptr<Self> This;
                /*
                _istream.first==nullptr： input,
                _ostream.second==nullptr： output,
                */
                class CellIStream
                {
                public:
                    std::pair<std::variant<Self *, std::shared_ptr<IStreamt>>, std::pair<Self *, std::shared_ptr<CellValuet>>> _iostream;
                    GTNAStatus _status;
                };
                class CellOStream
                {
                public:
                    std::pair<std::pair<Self *, std::shared_ptr<CellValuet>>, std::variant<Self *, std::shared_ptr<OStreamt>>> _iostream;
                    std::shared_ptr<StreamActivet> _active;
                    GTNAStatus _status;
                };
                class CellKernel
                {
                public:
                    std::pair<std::vector<CellIStream>, std::vector<CellOStream>> _iostream;
                    std::shared_ptr<CellActivet> _active;
                };
                friend NODE<Keys, Scales, Flow, NodeCtrls, NodeValues, NodeActives, LinkActives, CellCtrls, CellValues, CellActives, StreamActives, IStreams, OStreams>;

            private:
                Node *_root;
                Json _args;
                CellKernel _active;
                mutable std::tuple<GTNAStatus, std::thread, std::timed_mutex> _thread;
            };

            typedef std::shared_ptr<Node> This;
            typedef std::shared_ptr<CELL<Keys, Scales, Flows, CellValues, CellActives, StreamActives, IStreams, OStreams>> Leaf;

            class NodeKernel
            {
            public:
                /*
                _istream.first==nullptr： input,
                _ostream.second==nullptr： output,
                */
                class NodeILink
                {
                public:
                    std::pair<Node *, std::pair<Node *, std::shared_ptr<NodeValues>>> _iostream;
                    GTNAStatus _status;
                };
                class NodeOLink
                {
                public:
                    std::pair<std::pair<Node *, std::shared_ptr<NodeValues>>, Node *> _iostream;
                    std::shared_ptr<LinkActives> _active;
                    GTNAStatus _status;
                };

            private:
                std::pair<std::vector<NodeILink>, std::vector<NodeOLink>> _iostream;
                std::shared_ptr<NodeActives> _active;

            protected:
            public:
                template <typename NodeActivet>
                NodeKernel(const NodeActivet &act)
                {
                    _active = std::make_shared<NodeActives>(act);
                }
                std::shared_ptr<NodeActives>&getActive(){
                    return _active;
                }
                const std::shared_ptr<NodeActives>getActive()const{
                    return _active;
                }
                std::vector<NodeILink>&getILink(){
                    return _iostream.first;
                }
                const std::vector<NodeILink>getILink()const{
                    return _iostream.first;
                }
                std::vector<NodeOLink>&getOLink(){
                    return _iostream.second;
                }
                const std::vector<NodeOLink>getOLink()const{
                    return _iostream.second;
                }
            };
            friend TREE<Key, Scale, Flow, NodeCtrl, NodeValue, NodeActive, LinkActive, CellCtrl, CellValue, CellActive, StreamActive, IStream, OStream>;

        private:
            std::variant<std::map<Keys, std::pair<Leaf, std::shared_ptr<CellCtrl>>>,
                         std::map<Keys, std::pair<This, std::shared_ptr<NodeCtrl>>>>
                _next;
            std::variant<Tree *, Node *> _root;
            Json _args;
            NodeKernel _kernel;
            mutable std::tuple<GTNAStatus, std::thread, std::timed_mutex> _thread;

        protected:
            NODE(const std::string &args)
            {
                _args = Json::parse(args);
            }

        public:
            static std::shared_ptr<Node> New(const std::string &args)
            {
                return std::shared_ptr<Node>(new Node(args));
            }
            const Json getArgs() const
            {
                return _args;
            }
            Json &getArgs()
            {
                return _args;
            }
            const NodeKernel getKernel() const
            {
                return _kernel;
            }
            NodeKernel &getKernel()
            {
                return _kernel;
            }
            bool isRoot() const { return std::holds_alternative<Tree *>(_root) ? std::get<Tree *>(_root) == nullptr : std::get<Node *>(_root) = nullptr; }
            bool isBrach() const
            {
                return std::holds_alternative<std::map<Key, This>>(_next);
            }
            std::map<Keys, std::pair<This, std::shared_ptr<NodeCtrl>>> getBranch() const
            {
                return std::get<std::map<Keys, std::pair<This, std::shared_ptr<NodeCtrl>>>>(_next);
            }
            std::map<Keys, std::pair<This, std::shared_ptr<NodeCtrl>>> getBranch()
            {
                return std::holds_alternative<std::map<Keys, std::pair<This, std::shared_ptr<NodeCtrl>>>>(_next) ? std::get<std::map<Keys, std::pair<This, std::shared_ptr<NodeCtrl>>>>(_next) : std::map<Keys, std::pair<This, std::shared_ptr<NodeCtrl>>>();
            }
            std::map<Keys, std::pair<Leaf, std::shared_ptr<CellCtrl>>> getLeaf() const
            {
                return std::get<std::map<Keys, std::pair<Leaf, std::shared_ptr<CellCtrl>>>>(_next);
            }
            std::map<Keys, std::pair<Leaf, std::shared_ptr<CellCtrl>>> getLeaf()
            {
                return std::holds_alternative<std::map<Keys, std::pair<Leaf, std::shared_ptr<CellCtrl>>>>(_next) ? std::get<std::map<Keys, std::pair<Leaf, std::shared_ptr<CellCtrl>>>>(_next) : std::map<Keys, std::pair<Leaf, std::shared_ptr<CellCtrl>>>();
            }
            std::variant<Tree *, Node *> toRoot() const { return _root; }
            const Tree *baseRoot() const
            {

                return isRoot() ? this : (std::holds_alternative<Tree *>(_root) ? std::get<Tree *>(_root)->baseRoot() : std::get<Node *>(_root)->baseRoot());
            }
            const Node *gotoBranch(const std::string &path) const
            {
                if (path == "" || path == "." || path == "./" || path == "/./")
                    return this;
                auto npos = path.find_first_of("//");
                std::string expath = path.substr(0, npos), nextpath = path.substr(npos + 1);
                if (expath == "" || expath == "." || expath == "./" || expath == "/./")
                    return this->gotoBranch(nextpath);
                if (expath == "..")
                {
                    if (!isRoot() && std::holds_alternative<Node *>)
                        return std::get<Node *>(_root)->gotoBranch(nextpath);
                    else
                        return nullptr;
                }
                if (std::holds_alternative<std::map<Key, Node>>(_next))
                    return nullptr;
                auto it = std::get<std::map<Key, This>>(_next).find(expath);
                return it == std::get<std::map<Key, This>>(_next).end() ? nullptr : it->second.first->gotoBranch(nextpath);
            }
        };
        typedef std::shared_ptr<NODE<Key, Scale, Flow, NodeCtrl, NodeValue, NodeActive, LinkActive, CellCtrl, CellValue, CellActive, StreamActive, IStream, OStream>> Node;

    private:
        std::variant<std::map<Key, Node>,
                     std::map<Key, This>>
            _next;
        Tree *_root;
        Json _args;

    protected:
        TREE(const std::string &args)
        {
            _args = Json::parse(args);
        }

    public:
        static std::shared_ptr<Tree> New(const std::string &args)
        {
            return std::shared_ptr<Tree>(new Tree(args));
        }
        Json getArgs() const
        {
            return _args;
        }
        bool isRoot() const { return _root == nullptr; }
        bool isBrach() const
        {
            return std::holds_alternative<std::map<Key, This>>(_next);
        }
        std::map<Key, This> getBranch() const
        {
            return std::get<std::map<Key, This>>(_next);
        }
        std::map<Key, This> getBranch()
        {
            return std::holds_alternative<std::map<Key, This>>(_next) ? std::get<std::map<Key, This>>(_next) : std::map<Key, This>();
        }
        std::map<Key, Node> getNode() const
        {
            return std::get<std::map<Key, Node>>(_next);
        }
        std::map<Key, Node> getNode()
        {
            return std::holds_alternative<std::map<Key, Node>>(_next) ? std::get<std::map<Key, Node>>(_next) : std::map<Key, Node>();
        }
        Tree *fromRoot() const { return _root; }
        Tree *baseRoot() const { return _root == nullptr ? nullptr : (_root->_root == nullptr ? _root : _root->baseRoot()); }
        const Tree *gotoBranch(const std::string &path) const
        {
            if (path == "" || path == "." || path == "./" || path == "/./")
                return this;
            auto npos = path.find_first_of("//");
            std::string expath = path.substr(0, npos), nextpath = path.substr(npos + 1);
            if (expath == "" || expath == "." || expath == "./" || expath == "/./")
                return this->gotoBranch(nextpath);
            if (expath == "..")
                return _root ? _root->gotoBranch(nextpath) : nullptr;
            if (std::holds_alternative<std::map<Key, Node>>(_next))
                return nullptr;
            auto it = std::get<std::map<Key, This>>(_next).find(expath);
            return it == std::get<std::map<Key, This>>(_next).end() ? nullptr : it->second->gotoBranch(nextpath);
        }
    };
    template <typename Key, typename Scale, typename Flow>
    using KeyTree = TREE<Key, Scale, Flow,
                         NodeCtrl<Scale, Flow>, NodeValue<Scale, Flow>, NodeActive<Scale, Flow>, LinkActive<Scale, Flow>, CellCtrl<Scale, Flow>,
                         CellValue<Scale, Flow>, CellActive<Scale, Flow>, StreamActive<Scale, Flow>,
                         IStream<Scale, Flow>, OStream<Scale, Flow>>;
} // namespace exKF