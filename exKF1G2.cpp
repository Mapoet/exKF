
#include <iostream>
#include <random>
#include <numeric>
#include "mgl2/glut.h"
#include "exKF.hpp"
std::size_t dnf=0;
int animation2(const std::vector<std::vector<double>> & data){
    typedef std::vector<double> Vector;
  auto gl = new mglGLUT([](HMGL hmgr, void *v) {
        auto gr = new mglGraph(hmgr);
        std::vector<std::vector<double>> data = *((std::vector<std::vector<double>> *)v);
        std::vector<mglDataS> vs(data.size());
        auto n=data[0].size(),m=data.size();
        for (auto i = 0; i < n; i += 50)
        {
            for(auto j=i;j<(i+50>n?n:i+50);j++)
            for(auto k=0;k<m;k++)vs[k].push_back(data[k][j]);
            gr->NewFrame();
            gr->SetOrigin(0, 0);
            gr->SetRanges(0, 1500, 0, 1500);
            gr->Plot(vs[3], vs[4], "#.b1");
            gr->Plot(vs[5], vs[6], "xr1");
            gr->Plot(vs[7], vs[8], "+g1");
            gr->Plot(vs[1], vs[2], "+y1");
            gr->Grid();
            gr->Axis();
            gr->EndFrame();
        }
        return gr->GetNumFrame();
    },
                          "2D Kalman Filter", (void*)&data);
    return gl->Run();
}
int animation1(const std::vector<std::vector<double>> & data){
    typedef std::vector<double> Vector;
  auto gl = new mglGLUT([](HMGL hmgr, void *v) {
        auto gr = new mglGraph(hmgr);
        const std::string cls="wkrgbcymhRGBCYMHWlenupqLENUPQ";
        std::vector<std::vector<double>> data = *((std::vector<std::vector<double>> *)v);
        std::vector<mglDataS> vs(data.size());
        auto n=data[0].size(),m=data.size();
        char title[500];
        sprintf(title,"%5.5lu/%5.5lu/%5.5lu",dnf,n-dnf,n);
        for (auto i = 0; i < n; i += 5000)
        {
            for(auto j=i;j<(i+5000>n?n:i+5000);j++)
            for(auto k=0;k<m;k++)vs[k].push_back(data[k][j]);
            gr->NewFrame();
            gr->SetOrigin(0, 0);
            gr->SetRanges(*data[0].begin(), *data[0].rbegin(), 0, 1500);
            for(auto k=1;k<m;k++)
                gr->Plot(vs[0], vs[k], cls.substr(k,1).c_str());
            gr->Grid();
            gr->Axis();
            gr->Title(title);
            gr->EndFrame();
        }
        return gr->GetNumFrame();
    },
                          "2D Kalman Filter", (void*)&data);
    return gl->Run();
}

int main(int argc,char**argv){
    typedef exKF::exKF<double>    exKF;
    std::random_device rd;
    std::mt19937 gen(rd());
    std::normal_distribution<double> randn;
    double lambda=argc>=2?atof(argv[1]):0.01,beta=50;
    dnf=argc>=3?atoi(argv[2]):9000;
    exKF::Array  x0={0.0,0.0,0.1,0.2},p={0.0},theory={0.0,0.0},measure={0.0,0.0};
    exKF::Matrix P,Q,R, xc;
    std::vector<std::vector<double>> data(9);
    P.setZero(4,4);
    R.setZero(2,2);
    P(0,0)=1.0;
    P(1,1)=1.0;
    P(2,2)=1.0;
    P(3,3)=1.0;
    Q=lambda*P;
    R(0,0)=beta*beta;
    R(1,1)=beta*beta;
    exKF kf(x0,P,p,[](const exKF::DiffArray&argsin,const exKF::Array&parameter){
        exKF::DiffArray argsout(4);
        argsout[0]=argsin[0]+argsin[2]*parameter[1];
        argsout[1]=argsin[1]+argsin[3]*parameter[1];
        argsout[2]=argsin[2];
        argsout[3]=argsin[3];
        return argsout;
    });
    for (auto i=0;i<10000;i++){
        p[0]=0.1*i;
        kf.predict(p,Q);
        theory[0]=0.1*i*sin(p[0]/300*3.14159265)*(1+randn(gen)*0.0002)+0.1*i;
        theory[1]=0.2*i*sin(p[0]/500*3.14159265)*(1+randn(gen)*0.0002)+0.2*i;
        measure[0]=theory[0]+randn(gen)*beta;
        measure[1]=theory[1]+randn(gen)*beta;
        kf.update_linear(measure,R,[](const exKF::DiffArray&argsin,const exKF::Array&parameter){
        exKF::DiffArray argsout(2);
        argsout[0]=argsin[0];
        argsout[1]=argsin[1];
        return argsout;
    });
        xc=kf.getState();
        //std::cout<<theory[0]<<"\t"<<theory[1]<<"\t"<<measure[0]<<"\t"<<measure[1]<<"\t"<<xc(0,0)<<"\t"<<xc(1,0)<<"\n";
        data[0].emplace_back(p[0]);
        data[1].emplace_back(theory[0]);
        data[3].emplace_back(measure[0]);
        data[5].emplace_back(xc(0,0));
        data[7].emplace_back(xc(2,0));
        data[2].emplace_back(theory[1]);
        data[4].emplace_back(measure[1]);
        data[6].emplace_back(xc(1,0));
        data[8].emplace_back(xc(3,0));
    }
    for(int i=0;i<10000;i++){
        for(int j=0;j<8;j++)
        if(j==7)std::cout<<data[j][i]<<std::endl;else std::cout<<data[j][i]<<'\t';
    }
//    Eigen::AutoDiffJacobian<Jac<double>> a;
    // Eigen::FFT<double> fft;
    // typedef std::vector<std::complex<double>> CVector;
    // CVector freq;
    // fft.fwd(freq,data[3]);
    // for(std::size_t i=freq.size()-dnf;i<freq.size();i++)
    //    freq[i]=0.0;
    // fft.inv(data[7],freq);
    // fft.fwd(freq,data[4]);
    // for(std::size_t i=freq.size()-dnf;i<freq.size();i++)
    //    freq[i]=0.0;
    // fft.inv(data[8],freq);
   // animation1(data);
    animation2(data);
    return 0;
}