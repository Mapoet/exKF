#ifndef __KERNEL_H__
#define __KERNEL_H__
#include "Diff.hpp"
#include "exKF.hpp"
namespace exKF{

	template<typename Cell>
	class kernel{
		template<typename Scale, typename Flow>
		friend class FunctionalActive;
		template<typename Scale, typename Flow>
		friend class FunctionalTransmit;
        typedef std::valarray<Cell> Array;
        typedef std::valarray<Diff<Cell>> DiffArray;
        typedef Eigen::Matrix<Cell, -1, -1> Matrix;
        typedef DiffArray (*Apply)(const DiffArray &argsin, const Array &parameter);
	protected:
        Apply _apply;
		kernel(const std::string&name="",const Apply& apply=nullptr):_name(name),_apply(apply){}
	public:
        static std::shared_ptr<kernel<Cell>> New(const std::string&name="",const Apply& apply=nullptr){
            return std::shared_ptr<kernel<Cell>>(new kernel<Cell>(name,apply));
        }
		std::valarray<Diff<Cell>>
			apply(const size_t&itype, const size_t&otype, const std::valarray<autodiff<Cell>>& argsin){
				assert(itype <= argsin.size());
				return argsinout(itype,otype,argsin);
			}
	};
    
}
#endif