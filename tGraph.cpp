#define __cplusplus 201703L
#include <type_traits>
//#include <concepts>
#include <iostream>
#include <random>
#include <string>
#include <numeric>
#include "mgl2/glut.h"
#include "exKF.hpp"
#include "Graph.hpp"

// template<typename T>
// //requires std::is_arithmetic<T>::value
// requires false
// class Foo
// {
// }; 

int main(int argc,char**argv){

    typedef exKF::KeyTree<std::string,double,Eigen::Matrix<double,-1,-1>> TensorTree;
    auto tree=TensorTree::New("{\"a\":[2.3,-3.4,1.2,1.2]}");
    auto v=tree->getArgs();
    auto s="{\"a\":2.3,\"b\":4.5,\"x\":5.6}"_json;
    //tree->getNode()["1"]->getLeaf()["v"].second->_value;
    tree->getNode()["1"]->getBranch()["c"].first->getKernel().getILink()[0]._iostream.first->getKernel().getOLink()[1]._active;
    auto k=tree->gotoBranch("");
    std::cout<<v<<s<<std::endl;
    return 0;
}