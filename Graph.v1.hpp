#define __cplusplus 201703L
#include <memory>
#include <map>
#include <thread>
#include <tuple>
#include <variant>
#include <mutex>
#include "json.hpp"
namespace exKF
{
    typedef nlohmann::json Json;
    typedef enum __GTNAStatus
    {
        GTNA_Valued,
        GTNA_Trained
    } GTNAStatus;
    template <typename Key, typename NodeKernel, typename NodeCtrl, typename CellKernel>
    class CELL
    {
    public:
        typedef CELL<Key, NodeKernel, NodeCtrl, CellKernel> Self;
        typedef NODE<Key, NodeKernel, NodeCtrl, CellKernel> Node;
        typedef std::shared_ptr<Self> This;
        typedef Node *Root;

    private:
        NodeCtrl* _root;
        Json      _args;
        CellKernel _active;
        mutable std::tuple<GTNAStatus, std::thread, std::timed_mutex> _thread;
    };
    template <typename Key, typename NodeKernel, typename NodeCtrl, typename CellKernel>
    class NODE
    {
    public:
        typedef CELL<Key, NodeKernel, NodeCtrl, CellKernel> Cell;
        typedef NODE<Key, NodeKernel, NodeCtrl, CellKernel> Self;
        typedef TREE<Key, NodeKernel, NodeCtrl, CellKernel> Tree;
        typedef std::shared_ptr<Self> This;
        typedef std::shared_ptr<Cell> Leaf;

    private:
        std::variant<std::map<Key, Leaf>,
                     std::map<Key, This>>
            _next;
        std::variant<Tree *, Self *> _root;
        Json      _args;
        NodeKernel _active;
        NodeCtrl   _ctrl;
        mutable std::tuple<GTNAStatus, std::thread, std::timed_mutex> _thread;
    };
    template <typename Key, typename NodeKernel, typename NodeCtrl, typename CellKernel>
    class TREE
    {
    public:
        typedef std::shared_ptr<NODE<Key, NodeKernel, NodeCtrl, CellKernel>> Node;
        typedef TREE<Key, NodeKernel, NodeCtrl, CellKernel> Self;
        typedef std::shared_ptr<Self> This;

    private:
        std::variant<std::map<Key, Node>,
                     std::map<Key, This>>
            _next;
        Self *_root;
        Json      _args;
    };
    template<typename NodeActive, typename NodeILink, typename NodeOLink>
    class NodeKernel{
        std::pair<std::vector<NodeILink*>,std::vector<NodeOLink*>> _iostream;
        NodeActive _active;
    };
    template<typename Node>
    class NodeILink{
        std::pair<Node*,Node*> _iostream;
        GTNAStatus _status;
    };
    template<typename Scale,typename NodeValue,typename LinkActive,typename Node>
    class NodeOLink{
        std::pair<std::pair<Node*,NodeValue>,std::pair<Node*,NodeValue>> _iostream;
        LinkActive  _active;
        GTNAStatus _status;
    };
    template<typename Scale,typename Node,typename Cell,typename CtrlActive>
    class NodeCtrl{
        Node *_root;
        std::map<Cell*,CtrlActive> _nodes;
    };
    template<typename Scale,typename CellActive,typename CellIStream,typename CellOStream>
    class CellKernel{
        std::pair<std::vector<CellIStream*>,std::vector<CellOStream*>> _iostream;
        CellActive _active;
    };
    template<typename Scale,typename Cell>
    class CellIStream{
        std::pair<Cell*,Cell*> _iostream;
        GTNAStatus _status;
    };
    template<typename Scale,typename CellValue,typename StreamActive,typename Cell>
    class CellOStream{
        std::pair<std::pair<Cell*,CellValue>,std::pair<Cell*,CellValue>> _iostream;
        StreamActive  _active;
        GTNAStatus _status;
    };
    template<typename Scale,typename Flow>
    class NodeActive{
        Json _args;
    };
    template<typename Scale,typename Flow>
    class LinkActive{
        Json _value;
    };  
    template<typename Scale,typename Flow>
    class CellActive{
        Json _value;
    };  
    template<typename Scale,typename Flow>
    class StreamActive{
        Json _value;
    };  
    template<typename Scale,typename Flow>
    class NodeValue{
        Json _value;
    };   
    template<typename Scale,typename Flow>
    class CellValue{
        Json _value;
    };     

    template<typename Key,typename Scale,typename Flow>
    using KeyTree=TREE<Key,NodeKernel<NodeActive<Scale,Flow>,NodeILink<

} // namespace exKF